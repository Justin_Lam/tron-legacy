/*

 MSE 2202b Final Project
 Language: Arduino
 Authors: Tron: Legacy
 Date: 14/03/11
 
 --------------- 
 
 Information for integrating communication between the base and top
 0) Need to comment out all Serial communication because you can't use it at the same time as RX+TX
 1) the top board will be starting everything off (requires calibration), so the base should begin by waiting for a value of '0' from the top
 2) the top will send
 0: holding no mail
 1-4: mailroom 1-4
 3) after the top has sent something, it goes into it's 'waiting stage', only relevant numbers are included here
 0: waiting stage
 1: retrieval
 4: delivery stage
 4) therefore the base needs to send back '1' or '4' to the top
 
 Refer to Serial_Com_Test for more detail, but this is the basic breakdown of how to do it:
 1) take all of the code and stick it into case 1, and make case 0 a default case where it's waiting for a value from the top
 2) now the base should move to the necessary location depending on what it recieves
 3) send back a value to the top of what it should do, (retrieve or deliver)
 4) the base will need to keep track of things, like if it's holding mail at the moment
 
 example of communication:
 
 //we've arrived at the mailbox, not holding mail
 mail = 1; //set top to a value
 Serial.println(mail); //send it
 go = 0; //default case, wait for a response from the top
 
 //the top retrieved the mail
 
 //we're inside case 0, where go = 0
 int top =  Serial.parseInt(); //get the value from the top
 go = 1; //set go to 1
 mailbox = top; //set where we're going to top
 
 --------------- 
 
 REV 6 by Stephen
 //reintegrated the use of two ultrasonic sensors
 //properly recognizes gaps to follow the and allign with the wall
 
 
 REV 5 by Justin -
 //recognizes gaps and will continue to drive forward instead
 //excess code trimmed
 
 REV 4 by Stephen -
 //added switch cases for driving
 //added the back ultrasonic, commented out unneccessary code, 
 //changed the ports to use analog and digital for ultrasonic readings leaving 2 ports for the encoders
 //uses encoder values to turn the gap and find the wall afterwards
 
 
 REV 3 by Justin -
 //added the front ultrasonic, changed ports for all sensors
 
 REV 2 by Justin -
 //added some more cases for driving
 
 REV 1 by Justin - 
 //Wall tracking/following code
 //some extra code for later that is commented out
 //only trying to drive foward and backwards using the wall
 
 */

#include <Servo.h>
#include <EEPROM.h>
#include <uSTimer2.h>
#include <CharliePlexM.h>

//CharliePlexM::ul_LeftEncoder_Count;
//CharliePlexM::ul_RightEncoder_Count;
//int led = A5;

int currentBox = 1;
int nextBox = 0;
int incoming = 1;
int outgoing = 1;
int LightReading = 0;
int start = 0;

//======Encoder Settings=======

int gapDistance = 0;
int gapIncreaseBig = 370;
int gapIncreaseSmall = 360;

//From Mailbox 1 - 3:

int preTurn13 = 930;
int gapTurn13 = 460;
int postTurn13 = 1400;
int distance13 = 1570;

//From Mailbox 3-1:
int distance31 = 2700;
int preTurn31 = 1055;
int gapTurn31 = 1470;
int postTurn31 = 1700;
int preStop31 = 550;

//From Mailbox 1-2:
int distance12 = 5610;

//From Mailbox 2-1:
int distance21 = 4900;


//=============================

int stage = 0;
int gapCount = 0; 
int setDifference = 0;
int doorCount = 0;
boolean addtoDoor = false;
boolean goingForwards = true; //true: moving forwards //false: moving backwards

//light sensor variables
const int ci_Light_Sensor_Front = A4;
const int ci_Light_Sensor_Back = A5;

int LightSensorFront=0;
int LightSensorBack=0;

//microswitches (whiskers)
int whiskerPort1 = 11;
int whiskerPort2 = 12;

int whiskerCount1 = 0;
int whiskerCount2 = 0;

boolean whisker1;
boolean whisker2;
boolean whiskerOld1 = HIGH;
boolean whiskerOld2 = HIGH;

//declaration of motors and servos
Servo servo_LeftMotor;
Servo servo_RightMotor;
//Servo servo_BeltMotor;
//Servo servo_Bridge;

//declaration of values for motors and servos
int stopped = 1500;
int forward = 2000; //max 2000
int reverse = 1000; //min 1000
int slowForward = 1700;
int slowReverse = 1300;

//for minor adjustments in straightening
int turnForwards = 1350;
int turnBackwards = 1650;
int adjustForwards = 1580;
int adjustBackwards = 1420;

int leftClose = 8;
int leftFar = 8;
int leftGapClose = 13;
int leftGapFar = 14;

int Obstacle = 20;

//declarations for ultrasonic sensors distances
unsigned long u1_PingTime_LeftOne = 0;
unsigned long distance_LeftOne = 10;
unsigned long distance_LeftOneOld = 0;
long distance_LeftOneDiff = 0;
long distance_LeftOneDebounce = 0;

unsigned long u1_PingTime_LeftTwo = 0;
unsigned long distance_LeftTwo = 10;
unsigned long distance_LeftTwoOld = 0;
long distance_LeftTwoDiff = 0;
long distance_LeftTwoDebounce = 0;

unsigned long u1_PingTime_Front = 0;
unsigned long distance_Front = 10;
unsigned long distance_FrontOld = 0;
long distance_FrontDiff = 0;

unsigned long u1_PingTime_Back = 0;
unsigned long distance_Back = 10;
unsigned long distance_BackOld = 0;
long distance_BackDiff = 0;





//Encoder Declarations

int left_Encoder = 4;
int right_Encoder = 5;

int left_EncoderValue = 0;
int right_EncoderValue = 0;



// Uncomment keywords to enable debugging output

//#define DEBUG_MODE_DISPLAY
//#define DEBUG_MOTORS
//#define DEBUG_LINE_TRACKERS
//#define DEBUG_ENCODERS
//#define DEBUG_ULTRASONIC
//#define DEBUG_LINE_TRACKER_CALIBRATION
//#define DEBUG_MOTOR_CALIBRATION
//#define DEBUG_ARM
//#define DEBUG_DIFF
#define DEGUG_FRONT_US

boolean bt_Motors_Enabled = true;

//port pin constants
//this is the default setup from Lab 04 for the most part
const int ci_encoder_Pin_A[2] = {
  2,3};

const int ci_Ultrasonic_LeftOne = 6; //input plug
const int ci_Ultrasonic_LeftOne_Data = 7; //output plug
const int ci_Ultrasonic_LeftTwo = 2; //input plug //these two replaced the arm and grip
const int ci_Ultrasonic_LeftTwo_Data = 3; //output plug

//We need to reserve 2 digital pins for the encoders, thus, the front and back are declared to analog
const int ci_Ultrasonic_Front = A0; //input (trigger - black)
const int ci_Ultrasonic_Front_Data = A1; //output (echo - red)
const int ci_Ultrasonic_Back = A2; //input (trigger - black)
const int ci_Ultrasonic_Back_Data = A3; //output (echo - red)

const int ci_Mode_Button = 13; //we should move this to board two
const int ci_Left_Motor = 8;
const int ci_Right_Motor = 9;

const int ci_Charlieplex_LED1 = 4;
const int ci_Charlieplex_LED2 = 7;
const int ci_Charlieplex_LED3 = 12;
const int ci_Charlieplex_LED4 = 13;

//const int ci_Left_Line_Tracker = A0;
//const int ci_Middle_Line_Tracker = A1;
//const int ci_Right_Line_Tracker = A2;
//const int ci_Motor_Speed_Pot = A3;
//const int ci_Light_Sensor = A4;
//const int ci_Arm_Length_Pot = A5;

// Charlieplexing LED assignments
/*
const int ci_Left_Line_Tracker_LED = 1;
 const int ci_Middle_Line_Tracker_LED = 4;
 const int ci_Right_Line_Tracker_LED = 7;
 */
const int ci_Indicator_LED = 3;
const int ci_Heartbeat_LED = 12;

//constants

// EEPROM addresses
const int ci_Right_Motor_Offset_Address_L = 0;
const int ci_Right_Motor_Offset_Address_H = 1;
const int ci_Left_Motor_Offset_Address_L = 2;
const int ci_Left_Motor_Offset_Address_H = 3;

/*
const int ci_Left_Line_Tracker_Dark_Address_L = 4;
 const int ci_Left_Line_Tracker_Dark_Address_H = 5;
 const int ci_Left_Line_Tracker_Light_Address_L = 6;
 const int ci_Left_Line_Tracker_Light_Address_H = 7;
 const int ci_Middle_Line_Tracker_Dark_Address_L = 8;
 const int ci_Middle_Line_Tracker_Dark_Address_H = 9;
 const int ci_Middle_Line_Tracker_Light_Address_L = 10;
 const int ci_Middle_Line_Tracker_Light_Address_H = 11;
 const int ci_Right_Line_Tracker_Dark_Address_L = 12;
 const int ci_Right_Line_Tracker_Dark_Address_H = 13;
 const int ci_Right_Line_Tracker_Light_Address_L = 14;
 const int ci_Right_Line_Tracker_Light_Address_H = 15;
 */

const int ci_Left_Motor_Stop = 1500; // 200 for brake mode; 1500 for stop
const int ci_Right_Motor_Stop = 1500;

/*
const int ci_Grip_Motor_Open = 176; // Experiment to determine appropriate value
 const int ci_Grip_Motor_Zero = 90; // "
 const int ci_Grip_Motor_Closed = 140; // "
 const int ci_Arm_Pot_Retracted = 1020; // "
 const int ci_Arm_Pot_Extended = 400; // "
 const int ci_Arm_Pot_Tolerance = 60; // "
 */

const int ci_Display_Time = 500;
const int ci_Num_Encoders = 2;
const int ci_Encoder_Steps_Per_Revolution = 90;
/*
const int ci_Line_Tracker_Calibration_Interval = 100;
 const int ci_Line_Tracker_Cal_Measures = 20;
 */
const int ci_Motor_Calibration_Time = 5000;

//variables
byte b_LowByte;
byte b_HighByte;
unsigned long ul_Echo_Time_LeftOne;
unsigned long ul_Echo_Time_LeftTwo;
unsigned long ul_Echo_Time_Front;
unsigned long ul_Echo_Time_Back;
/*
unsigned int ui_Left_Line_Tracker_Data;
 unsigned int ui_Middle_Line_Tracker_Data;
 unsigned int ui_Right_Line_Tracker_Data;
 */
unsigned int ui_Motors_Speed;
unsigned int ui_Left_Motor_Speed;
unsigned int ui_Right_Motor_Speed;
//unsigned int ui_Arm_Length_Data;

volatile unsigned long ul_encoder_Count[ci_Num_Encoders] = {
  0,0};
unsigned long ul_encoder_Pos[ci_Num_Encoders] = {
  0,0};
unsigned long ul_old_Encoder_Pos[ci_Num_Encoders] = {
  0,0};

unsigned long ul_3_Second_timer = 0;
unsigned long ul_Display_Time;
unsigned long ul_Calibration_Time;

unsigned int ui_Right_Motor_Offset = 0;
unsigned int ui_Left_Motor_Offset = 0;

unsigned int ui_Cal_Count;
/*
unsigned int ui_Left_Line_Tracker_Dark;
 unsigned int ui_Left_Line_Tracker_Light;
 unsigned int ui_Middle_Line_Tracker_Dark;
 unsigned int ui_Middle_Line_Tracker_Light;
 unsigned int ui_Right_Line_Tracker_Dark;
 unsigned int ui_Right_Line_Tracker_Light;
 unsigned int ui_Line_Tracker_Tolerance = 10;
 */

unsigned int ui_Robot_State_Index = 0;
//0123456789ABCDEF
unsigned int ui_Mode_Indicator[6] = {
  0x00, //B0000000000000000, //stop
  0x00FF, //B0000000011111111, //Run
  0x0F0F, //B0000111100001111, //Calibrate line tracker light level
  0x3333, //B0011001100110011, //Calibrate line tracker dark level
  0xAAAA, //B1010101010101010, //Calibrate motors
  0xFFFF}; //B1111111111111111};
unsigned int ui_Mode_Indicator_Index = 0;

//display Bits 0,1,2,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
int iArray[16] = {
  1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,65536};
int iArrayIndex = 0;


int i_Left_Motor_Calibration = 0;
int i_Right_Motor_Calibration = 0;

boolean bt_Heartbeat = true;
boolean bt_3_S_Time_Up = false;
boolean bt_Do_Once = false;
boolean bt_Cal_Initialized = false;

void setup(){
  Serial.begin(9600);

  //pinMode(led, OUTPUT);

  //Microswitch / whisker pull up

  pinMode(whiskerPort1, INPUT_PULLUP);
  pinMode(whiskerPort2, INPUT_PULLUP);  

  //CharliePlexM::setBtn(ci_Charlieplex_LED1,ci_Charlieplex_LED2,ci_Charlieplex_LED3,ci_Charlieplex_LED4,ci_Mode_Button);

  CharliePlexM::setEncoders(left_Encoder, right_Encoder);
  left_EncoderValue = CharliePlexM::ul_LeftEncoder_Count;
  right_EncoderValue = CharliePlexM::ul_RightEncoder_Count;

  // set up ultrasonic Left #1
  pinMode(ci_Ultrasonic_LeftOne, OUTPUT);
  pinMode(ci_Ultrasonic_LeftOne_Data, INPUT);

  // set up ultrasonic Left #2
  pinMode(ci_Ultrasonic_LeftTwo, OUTPUT);
  pinMode(ci_Ultrasonic_LeftTwo_Data, INPUT);

  // set up ultrasonic front
  pinMode(ci_Ultrasonic_Front, OUTPUT);
  pinMode(ci_Ultrasonic_Front_Data, INPUT);

  // set up ultrasonic back
  pinMode(ci_Ultrasonic_Back, OUTPUT);
  pinMode(ci_Ultrasonic_Back_Data, INPUT);

  // set up drive motors
  pinMode(ci_Left_Motor, OUTPUT);
  servo_LeftMotor.attach(ci_Left_Motor);
  pinMode(ci_Right_Motor, OUTPUT);
  servo_RightMotor.attach(ci_Right_Motor);

  /*

   // set up arm motors
   pinMode(ci_Arm_Motor, OUTPUT);
   servo_ArmMotor.attach(ci_Arm_Motor);
   pinMode(ci_Grip_Motor, OUTPUT);
   servo_GripMotor.attach(ci_Grip_Motor);
   servo_GripMotor.write(ci_Grip_Motor_Zero);
   
   */
  /*
  pinMode(ci_Middle_Line_Tracker, INPUT);
   pinMode(ci_Left_Line_Tracker, INPUT);
   pinMode(ci_Right_Line_Tracker, INPUT);
   
   
   pinMode(ci_Motor_Speed_Pot, INPUT);
   pinMode(ci_Arm_Length_Pot, INPUT);
   */

  // read saved values from EEPROM
  b_LowByte = EEPROM.read(ci_Right_Motor_Offset_Address_L);
  b_HighByte = EEPROM.read(ci_Right_Motor_Offset_Address_H);
  ui_Right_Motor_Offset = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Left_Motor_Offset_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Motor_Offset_Address_H);
  ui_Left_Motor_Offset = word(b_HighByte, b_LowByte);

  /*
  b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Left_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
   b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Light_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Left_Line_Tracker_Light = word(b_HighByte, b_LowByte);
   b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Dark_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Middle_Line_Tracker_Dark = word(b_HighByte, b_LowByte); 
   b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Light_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Middle_Line_Tracker_Light = word(b_HighByte, b_LowByte);
   b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Dark_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Right_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
   b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Light_Address_L);
   b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
   ui_Right_Line_Tracker_Light = word(b_HighByte, b_LowByte);
   ui_Line_Tracker_Tolerance = 75;
   */
}

void loop(){

  /*
  do
   {
   Ping_Left();
   Ping_Back();
   LightSensorFront = analogRead(ci_Light_Sensor_Front);
   LightSensorBack = analogRead(ci_Light_Sensor_Back);   
   //Serial.print("Left Sensor One: ");
   //Serial.println(distance_LeftOne);
   
   //Serial.print("Left Sensor Two: ");
   //Serial.println(distance_LeftTwo);
   
   Serial.print("Back Sensor:");
   Serial.println(distance_Back);
   Serial.print("Debounce");
   Serial.println(Debounce_Back());
   
   Serial.print("Light Sensor Front:");
   Serial.println(LightSensorFront);
   Serial.print("Light Sensor Back:");
   Serial.println(LightSensorBack);          
   
   
   
   servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
   servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);  
   
   }while (ul_Echo_Time_LeftOne > 0);*/


  switch (stage)
  {

  case 0: //stop and await mail readings to determine where to deliver
    {
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
      CharliePlexM::ul_LeftEncoder_Count = 0;
      CharliePlexM::ul_RightEncoder_Count = 0;    
      nextBox = Serial.parseInt();

      if (currentBox == 1 && nextBox == 69 && start == 0)
      {
        Serial.println(outgoing);
        start++;
      }

      if (currentBox == 1 && nextBox == 70 && start > 0)
      {
        stage = 1;
        nextBox = 3;
        outgoing = 4;
      }

      if (currentBox == 1 && nextBox == 2)
      {
        stage = 13;
        outgoing = 4;
      }

      if (currentBox == 1 && nextBox == 3)
      {
        stage = 1;
        outgoing = 4;
      }

      if (currentBox == 2 && nextBox == 1)
      {
        stage = 17;
        outgoing = 4;
      }

      if (currentBox == 2 && nextBox == 3)
      {
        stage = 17;
        outgoing = 4;
      }

      if (currentBox == 3 && nextBox == 1)
      {
        stage = 7;
        outgoing = 4;
      }

      if (currentBox == 3 && nextBox == 2)
      {
        stage = 7;
        outgoing = 4;
      }

      break;
    }

  case 1: //drive forward (begining of 1 - 3)
    {

      if (setDifference == 0) {
        distance_LeftOneOld = distance_LeftOne;
        setDifference++;
      }

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();      
      /*  
       left_EncoderValue = 0;
       right_EncoderValue = 0;
       CharliePlexM::ul_LeftEncoder_Count = 0;
       CharliePlexM::ul_RightEncoder_Count = 0;
       */

      if (CharliePlexM::ul_LeftEncoder_Count < 50 && CharliePlexM::ul_RightEncoder_Count < 50) 
      {
        servo_LeftMotor.writeMicroseconds(slowForward); 
        servo_RightMotor.writeMicroseconds(slowForward);         
      }

      else if (CharliePlexM::ul_LeftEncoder_Count < preTurn13 && CharliePlexM::ul_RightEncoder_Count < preTurn13)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();


          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseBig;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftOne is too far but LeftTwo is not , turn left slowly
              if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);   
              }

              //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
              else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);   
              }          

              //if LeftOne is too close, turn right
              else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(adjustForwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(forward);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(forward);
            }

          }

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= preTurn13 && CharliePlexM::ul_RightEncoder_Count <= preTurn13);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;       
        stage = 2;
        //delay (1000);        
      }

      break;
    }

  case 2: //turn the gap going forward (right turn)
    {

      if (CharliePlexM::ul_LeftEncoder_Count < gapTurn13)
      {

        do{
          //digitalWrite(led, LOW);
          Ping_Front();


          servo_LeftMotor.writeMicroseconds(forward); 
          servo_RightMotor.writeMicroseconds(turnForwards);

          if (distance_Front < Obstacle && distance_Front != 0){

            if (Debounce_Front()){

              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());

            }
          }      

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= gapTurn13);

      }
      else if (CharliePlexM::ul_LeftEncoder_Count > gapTurn13 && CharliePlexM::ul_LeftEncoder_Count < postTurn13)
      {

        do{
          Ping_Front();


          servo_LeftMotor.writeMicroseconds(forward); 
          servo_RightMotor.writeMicroseconds(forward);

          if (distance_Front < Obstacle && distance_Front != 0){

            if (Debounce_Front()){

              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());

            }
          }      


        }
        while (CharliePlexM::ul_LeftEncoder_Count < postTurn13);

      }

      else if ( CharliePlexM::ul_LeftEncoder_Count >= postTurn13)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;       
        stage = 3;        
      }

      break;
    }

  case 3: //find the wall and go to the third mailbox (roughly)
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();
      whiskerCount1 = 0;
      whiskerCount2 = 0;
      whiskerOld1 = HIGH;
      whiskerOld2 = HIGH;      


      if (CharliePlexM::ul_LeftEncoder_Count < distance13 && CharliePlexM::ul_RightEncoder_Count < distance13)
      {
        do
        {
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();          

          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftOne is too far but LeftTwo is not , turn left slowly
              if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);          
              }

              //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
              else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);   
              }          

              //if LeftOne is too close, turn right
              else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(adjustForwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(forward);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);  
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(forward);
            }

          }

        } 
        while (CharliePlexM::ul_LeftEncoder_Count < distance13 && CharliePlexM::ul_RightEncoder_Count < distance13);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;
        stage = 4;       
        //delay (2000);        
      }


      break;
    }

  case 4://drive up and align with incoming slot 3
    {

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();    


      if (whisker1 == HIGH || whiskerCount1 < 2)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();         
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();        

          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftOne is too far but LeftTwo is not , turn left slowly
              if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);          
              }

              //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
              else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);
              }          

              //if LeftOne is too close, turn right
              else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(adjustForwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(slowForward); 
                servo_RightMotor.writeMicroseconds(slowForward);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);  
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 2);
      }

      else if (whisker1 == LOW || whiskerCount1 == 2)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;

        stage = 5;        
      }      

      break;
    }

  case 5: //drop off mail and read light sensors
    { 
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);

      OutgoingLight();

      if (outgoing == 4)
      {
        Serial.println(outgoing);
      }

      stage = 21;
      break;
    }  

  case 6: //align with outgoing slot of mailbox 3
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();    


      if (whisker1 == HIGH || whiskerCount1 < 3)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();         
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();


          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          /* else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftOne is too far but LeftTwo is not , turn left slowly
           if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");           
           }
           
           //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
           else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");   
           }          
           
           //if LeftOne is too close, turn right
           else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(adjustForwards);
           // Serial.print("Gap Turn Right");
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(slowForward);
           //  Serial.print("Gap Forward");
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance); 
           }*/

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
              //    Serial.print("Turn Right");
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
              //   Serial.print("Forward");
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 3);
      }

      else if (whisker1 == LOW || whiskerCount1 == 3)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;

        if (LightReading == 0)
        {
          nextBox = 1;
          stage = 7;        
        }

        if (LightReading == 1)
        {
          Serial.println(outgoing);
          stage = 0;
        }
      }      

      break;  
    }


  case 7: //drive backward, start of travel from mailbox 3 - 1
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Back();      

      if (CharliePlexM::ul_LeftEncoder_Count < distance31 && CharliePlexM::ul_RightEncoder_Count < distance31)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Back();

          if (distance_Back < Obstacle && distance_Back != 0){
            if (Debounce_Back()){
              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());
            }
          }

          else if (distance_LeftTwo >= leftGapClose && distance_LeftOne <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftTwo is too far but LeftOne is not , turn 'left' slowly
              if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapFar && distance_LeftOne > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(reverse);   
              }

              //if LeftTwo is too far and LeftOne is too close or too far, turn 'left' faster
              else if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapClose || distance_LeftOne > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(reverse);   
              }          

              //if LeftTwo is too close, turn right
              else if (distance_LeftTwo < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(adjustBackwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(reverse);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftTwo is too far but LeftOne is not , turn left slowly
            if (distance_LeftTwo > leftFar && (distance_LeftOne < leftFar && distance_LeftOne > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }

            //if LeftTwo is too far and LeftOne is too close, turn left faster
            else if (distance_LeftTwo > leftFar && (distance_LeftOne < leftClose || distance_LeftOne > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }          

            //if LeftTwo is too close, turn right
            else if (distance_LeftTwo < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(adjustBackwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(reverse);
            }

          }

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= distance31 && CharliePlexM::ul_RightEncoder_Count <= distance31);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;       
        stage = 8;
        //delay (1000);        
      }

      break;      
    }

  case 8: //turn gap backwards ('right' turn)
    {

      if (CharliePlexM::ul_LeftEncoder_Count < preTurn31)
      {

        do{
          Ping_Back();

          servo_LeftMotor.writeMicroseconds(reverse); 
          servo_RightMotor.writeMicroseconds(reverse);

          if (distance_Back < Obstacle && distance_Back != 0){

            if (Debounce_Back()){

              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Front());

            }
          }      


        }
        while (CharliePlexM::ul_LeftEncoder_Count < preTurn31);
      }      

      else if (CharliePlexM::ul_LeftEncoder_Count > preTurn31 && CharliePlexM::ul_LeftEncoder_Count < gapTurn31)
      {

        do{
          Ping_Back();

          servo_LeftMotor.writeMicroseconds(reverse); 
          servo_RightMotor.writeMicroseconds(turnBackwards);

          if (distance_Back < Obstacle && distance_Back != 0){

            if (Debounce_Back()){

              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());

            }
          }      

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= gapTurn31);

      }

      else if (CharliePlexM::ul_LeftEncoder_Count > gapTurn31 && CharliePlexM::ul_LeftEncoder_Count < postTurn31)
      {

        do{
          Ping_Front();

          servo_LeftMotor.writeMicroseconds(reverse); 
          servo_RightMotor.writeMicroseconds(reverse);

          if (distance_Front < Obstacle && distance_Front != 0){

            if (Debounce_Front()){

              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());

            }
          }      


        }
        while (CharliePlexM::ul_LeftEncoder_Count < postTurn31);

      }

      else if ( CharliePlexM::ul_LeftEncoder_Count >= postTurn31)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;       
        stage = 9;        
      }



      break;
    }

  case 9: // find wall and get to mailbox 1 (roughly)

    {

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Back();
      whiskerCount1 = 0;
      whiskerCount2 = 0;
      whiskerOld1 = HIGH;
      whiskerOld2 = HIGH;      


      if (CharliePlexM::ul_LeftEncoder_Count < preStop31 && CharliePlexM::ul_RightEncoder_Count < preStop31)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Back();


          if (distance_Back < Obstacle && distance_Back != 0){
            if (Debounce_Back()){
              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());
            }
          }

          else if (distance_LeftTwo >= leftGapClose && distance_LeftOne <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseBig;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftTwo is too far but LeftOne is not , turn 'left' slowly
              if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapFar && distance_LeftOne > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(slowReverse);   
              }

              //if LeftTwo is too far and LeftOne is too close or too far, turn 'left' faster
              else if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapClose || distance_LeftOne > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(slowReverse);   
              }          

              //if LeftTwo is too close, turn right
              else if (distance_LeftTwo < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(adjustBackwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(reverse);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftTwo is too far but LeftOne is not , turn left slowly
            if (distance_LeftTwo > leftFar && (distance_LeftOne < leftFar && distance_LeftOne > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(slowReverse);   
            }

            //if LeftTwo is too far and LeftOne is too close, turn left faster
            else if (distance_LeftTwo > leftFar && (distance_LeftOne < leftClose || distance_LeftOne > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(slowReverse);   
            }          

            //if LeftTwo is too close, turn right
            else if (distance_LeftTwo < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(adjustBackwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(reverse);
            }

          }

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= preStop31 && CharliePlexM::ul_RightEncoder_Count <= preStop31);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;
        stage = 10;       

        //delay (1000);        
      }      

      break;
    }

  case 10: //align with mailbox 1 incoming
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Back();      
      //whisker2 = digitalRead (whiskerPort2);
      whiskerCounter2(); 

      if (whisker2 == HIGH || whiskerCount2 < 3)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Back();          
          //whisker2 = digitalRead (whiskerPort2);
          whiskerCounter2();           


          if (distance_Back < Obstacle && distance_Back != 0){
            if (Debounce_Back()){
              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());
            }
          }

          /* else if (distance_LeftTwo >= leftGapClose && distance_LeftOne <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftTwo is too far but LeftOne is not , turn 'left' slowly
           if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapFar && distance_LeftOne > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustBackwards); 
           servo_RightMotor.writeMicroseconds(slowReverse);   
           }
           
           //if LeftTwo is too far and LeftOne is too close or too far, turn 'left' faster
           else if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapClose || distance_LeftOne > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustBackwards); 
           servo_RightMotor.writeMicroseconds(slowReverse);   
           }          
           
           //if LeftTwo is too close, turn right
           else if (distance_LeftTwo < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowReverse); 
           servo_RightMotor.writeMicroseconds(adjustBackwards);
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowReverse); 
           servo_RightMotor.writeMicroseconds(slowReverse);
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
           } */

          else{

            //if LeftTwo is too far but LeftOne is not , turn left slowly
            if (distance_LeftTwo > leftFar && (distance_LeftOne < leftFar && distance_LeftOne > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }

            //if LeftTwo is too far and LeftOne is too close, turn left faster
            else if (distance_LeftTwo > leftFar && (distance_LeftOne < leftClose || distance_LeftOne > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }          

            //if LeftTwo is too close, turn right
            else if (distance_LeftTwo < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(adjustBackwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowReverse); 
              servo_RightMotor.writeMicroseconds(slowReverse);
            }

          }

        }
        while (whisker2 == HIGH || whiskerCount2 < 3);
      }

      else if (whisker2 == LOW || whiskerCount2 == 3)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;

        if (currentBox == 3 && nextBox == 2)
        {
          stage = 12;
          LightReading = 0;
        }

        else stage = 11;  
      } 

      break;  
    }

  case 11: //read light sensors
    {
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);

      OutgoingLight();

      if (outgoing == 4)
      {
        Serial.println(outgoing);
      }

      stage = 21;
      break;
    }

  case 12: // align with mailbox 1 outgoing
    {

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();      


      if (whisker1 == HIGH || whiskerCount1 < 1)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();       
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();



          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }
          /*
          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftOne is too far but LeftTwo is not , turn left slowly
           if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");           
           }
           
           //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
           else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");   
           }          
           
           //if LeftOne is too close, turn right
           else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(adjustForwards);
           // Serial.print("Gap Turn Right");
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(slowForward);
           //  Serial.print("Gap Forward");
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
           } */

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
              //    Serial.print("Turn Right");
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
              //   Serial.print("Forward");
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 1);
      }

      else if (whisker1 == LOW || whiskerCount1 == 1)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;

        if (currentBox == 3 && nextBox == 2)
        {
          stage = 13;        
        }

        else if (LightReading == 0)
        {
          nextBox = 2;
          stage = 13;        
        }

        else if (LightReading == 1)
        {
          Serial.println(outgoing);
          stage = 0;
        }     
      }    

      break;  
    }

  case 13: // go straight from 1 - 2:
    {

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Back();
      whiskerCount1 = 0;
      whiskerCount2 = 0;
      whiskerOld1 = HIGH;
      whiskerOld2 = HIGH;      


      if (CharliePlexM::ul_LeftEncoder_Count < distance12 && CharliePlexM::ul_RightEncoder_Count < distance12)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Back();

          if (distance_Back < Obstacle && distance_Back != 0){
            if (Debounce_Back()){
              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());
            }
          }

          else if (distance_LeftTwo >= leftGapClose && distance_LeftOne <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftTwo is too far but LeftOne is not , turn 'left' slowly
              if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapFar && distance_LeftOne > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(reverse);   
              }

              //if LeftTwo is too far and LeftOne is too close or too far, turn 'left' faster
              else if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapClose || distance_LeftOne > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustBackwards); 
                servo_RightMotor.writeMicroseconds(reverse);   
              }          

              //if LeftTwo is too close, turn right
              else if (distance_LeftTwo < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(adjustBackwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(reverse); 
                servo_RightMotor.writeMicroseconds(reverse);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftTwo is too far but LeftOne is not , turn left slowly
            if (distance_LeftTwo > leftFar && (distance_LeftOne < leftFar && distance_LeftOne > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }

            //if LeftTwo is too far and LeftOne is too close, turn left faster
            else if (distance_LeftTwo > leftFar && (distance_LeftOne < leftClose || distance_LeftOne > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }          

            //if LeftTwo is too close, turn right
            else if (distance_LeftTwo < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(adjustBackwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(reverse);
            }

          }

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= distance12 && CharliePlexM::ul_RightEncoder_Count <= distance12);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;       
        stage = 14;       
      }      


      break;
    }

  case 14: // drive up and allign with the incoming mail slot 2

    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Back();      
      //whisker2 = digitalRead (whiskerPort2);
      whiskerCounter2(); 

      if (whisker2 == HIGH || whiskerCount2 < 3)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Back();          
          //whisker2 = digitalRead (whiskerPort2);
          whiskerCounter2();           


          if (distance_Back < Obstacle && distance_Back != 0){
            if (Debounce_Back()){
              do{
                Ping_Back();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Back < Obstacle && distance_Back != 0 && Debounce_Back());
            }
          }

          /* else if (distance_LeftTwo >= leftGapClose && distance_LeftOne <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftTwo is too far but LeftOne is not , turn 'left' slowly
           if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapFar && distance_LeftOne > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustBackwards); 
           servo_RightMotor.writeMicroseconds(slowReverse);   
           }
           
           //if LeftTwo is too far and LeftOne is too close or too far, turn 'left' faster
           else if (distance_LeftTwo > leftGapFar && (distance_LeftOne < leftGapClose || distance_LeftOne > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustBackwards); 
           servo_RightMotor.writeMicroseconds(slowReverse);   
           }          
           
           //if LeftTwo is too close, turn right
           else if (distance_LeftTwo < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowReverse); 
           servo_RightMotor.writeMicroseconds(adjustBackwards);
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowReverse); 
           servo_RightMotor.writeMicroseconds(slowReverse);
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
           } */

          else{

            //if LeftTwo is too far but LeftOne is not , turn left slowly
            if (distance_LeftTwo > leftFar && (distance_LeftOne < leftFar && distance_LeftOne > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }

            //if LeftTwo is too far and LeftOne is too close, turn left faster
            else if (distance_LeftTwo > leftFar && (distance_LeftOne < leftClose || distance_LeftOne > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustBackwards); 
              servo_RightMotor.writeMicroseconds(reverse);   
            }          

            //if LeftTwo is too close, turn right
            else if (distance_LeftTwo < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(reverse); 
              servo_RightMotor.writeMicroseconds(adjustBackwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowReverse); 
              servo_RightMotor.writeMicroseconds(slowReverse);
            }

          }

        }
        while (whisker2 == HIGH || whiskerCount2 < 3);
      }

      else if (whisker2 == LOW || whiskerCount2 == 3)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;
        stage = 15;

      }      

      break; 
    }

  case 15: // read light sensors
    {  
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);

      OutgoingLight();

      if (outgoing == 4)
      {
        Serial.println(outgoing);
      }

      stage = 21;
      break;
    }

  case 16: //go from incoming mailbox to outgoing mailbox
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();      

      if (whisker1 == HIGH || whiskerCount1 < 1)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();       
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();

          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          /* else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftOne is too far but LeftTwo is not , turn left slowly
           if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");           
           }
           
           //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
           else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");   
           }          
           
           //if LeftOne is too close, turn right
           else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(adjustForwards);
           // Serial.print("Gap Turn Right");
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(slowForward);
           //  Serial.print("Gap Forward");
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
           }*/

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
              //    Serial.print("Turn Right");
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
              //   Serial.print("Forward");
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 1);
      }

      else if (whisker1 == LOW || whiskerCount1 == 1)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;   

        if (LightReading == 0)
        {
          nextBox = 1;
          stage = 17;        
        }

        if (LightReading == 1)
        {
          Serial.println(outgoing);
          stage = 0;
        }        
      }    

      break;
    }

  case 17: //go straight from 2 - 1:
    {

      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();
      whiskerCount1 = 0;
      whiskerCount2 = 0;
      whiskerOld1 = HIGH;
      whiskerOld2 = HIGH;      


      if (CharliePlexM::ul_LeftEncoder_Count < distance21 && CharliePlexM::ul_RightEncoder_Count < distance21)
      {
        do
        {
          //digitalWrite(led, LOW);
          //initially set distanceOld to the starting distance

          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();       

          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftOne is too far but LeftTwo is not , turn left slowly
              if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);   
              }

              //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
              else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);   
              }          

              //if LeftOne is too close, turn right
              else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(adjustForwards);
              }              

              else {
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(forward);
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(forward);
            }

          }

        }
        while (CharliePlexM::ul_LeftEncoder_Count <= distance21 && CharliePlexM::ul_RightEncoder_Count <= distance21);
      }

      else
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;         
        stage = 18;       

      }


      break;
    }

  case 18: // drive up and allign with the incoming mail slot 1
    {


      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();    


      if (whisker1 == HIGH || whiskerCount1 < 2)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();         
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();       

          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 

            gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;

            do{
              Ping_Left();
              //digitalWrite(led, HIGH);

              //if LeftOne is too far but LeftTwo is not , turn left slowly
              if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);
                // Serial.print("Gap Turn Left");           
              }

              //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
              else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(adjustForwards); 
                servo_RightMotor.writeMicroseconds(forward);
                // Serial.print("Gap Turn Left");   
              }          

              //if LeftOne is too close, turn right
              else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
                servo_LeftMotor.writeMicroseconds(forward); 
                servo_RightMotor.writeMicroseconds(adjustForwards);
                // Serial.print("Gap Turn Right");
              }              

              else {
                servo_LeftMotor.writeMicroseconds(slowForward); 
                servo_RightMotor.writeMicroseconds(slowForward);
                //  Serial.print("Gap Forward");
              }


            }
            while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance);
          }

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
              //    Serial.print("Turn Right");
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
              //   Serial.print("Forward");
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 2);
      }

      else if (whisker1 == LOW || whiskerCount1 == 2)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;
        if (currentBox == 2 && nextBox == 3)
        {
          stage = 20;
          LightReading = 0;
        }

        else stage = 19;         
      }      

      break; 
    }

  case 19: //read light sensors
    { 
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);

      OutgoingLight();

      if (outgoing == 4)
      {
        Serial.println(outgoing);
      }

      stage = 21;
      break;
    }  

  case 20: //align with outgoing slot of mailbox 1
    {
      Ping_Left();     
      //DifferenceLeftOne();
      Ping_Front();

      //whisker1 = digitalRead (whiskerPort1);
      whiskerCounter1();    


      if (whisker1 == HIGH || whiskerCount1 < 3)
      {
        do
        {
          //whisker1 = digitalRead (whiskerPort1);
          whiskerCounter1();         
          //digitalWrite(led, LOW);
          Ping_Left();     
          //DifferenceLeftOne();
          Ping_Front();


          if (distance_Front < Obstacle && distance_Front != 0){
            if (Debounce_Front()){
              do{
                Ping_Front();  
                servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
                servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
              }
              while (distance_Front < Obstacle && distance_Front != 0 && Debounce_Front());
            }
          }

          /* else if (distance_LeftOne >= leftGapClose && distance_LeftTwo <= leftFar){ 
           
           gapDistance = CharliePlexM::ul_LeftEncoder_Count + gapIncreaseSmall;
           
           do{
           Ping_Left();
           //digitalWrite(led, HIGH);
           
           //if LeftOne is too far but LeftTwo is not , turn left slowly
           if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapFar && distance_LeftTwo > leftGapClose) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");           
           }
           
           //if LeftOne is too far and LeftTwo is too close or too far, turn left faster
           else if (distance_LeftOne > leftGapFar && (distance_LeftTwo < leftGapClose || distance_LeftTwo > leftGapFar) && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(adjustForwards); 
           servo_RightMotor.writeMicroseconds(slowForward);
           // Serial.print("Gap Turn Left");   
           }          
           
           //if LeftOne is too close, turn right
           else if (distance_LeftOne < leftGapClose && Debounce_Left()){ 
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(adjustForwards);
           // Serial.print("Gap Turn Right");
           }              
           
           else {
           servo_LeftMotor.writeMicroseconds(slowForward); 
           servo_RightMotor.writeMicroseconds(slowForward);
           //  Serial.print("Gap Forward");
           }
           
           
           }
           while(CharliePlexM::ul_LeftEncoder_Count <= gapDistance && CharliePlexM::ul_RightEncoder_Count <= gapDistance); 
           }*/

          else{

            //if LeftOne is too far but LeftTwo is not , turn left slowly
            if (distance_LeftOne > leftFar && (distance_LeftTwo < leftFar && distance_LeftTwo > leftClose) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }

            //if LeftOne is too far and LeftTwo is too close, turn left faster
            else if (distance_LeftOne > leftFar && (distance_LeftTwo < leftClose || distance_LeftTwo > leftFar) && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(adjustForwards); 
              servo_RightMotor.writeMicroseconds(forward);
              // Serial.print("Turn Left");   
            }          

            //if LeftOne is too close, turn right
            else if (distance_LeftOne < leftClose && Debounce_Left()){ 
              servo_LeftMotor.writeMicroseconds(forward); 
              servo_RightMotor.writeMicroseconds(adjustForwards);
              //    Serial.print("Turn Right");
            }

            //else, drive straight
            else {
              servo_LeftMotor.writeMicroseconds(slowForward); 
              servo_RightMotor.writeMicroseconds(slowForward);
              //   Serial.print("Forward");
            }

          }

        } 
        while (whisker1 == HIGH || whiskerCount1 < 3);
      }

      else if (whisker1 == LOW || whiskerCount1 == 3)
      {
        servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
        servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
        CharliePlexM::ul_LeftEncoder_Count = 0;
        CharliePlexM::ul_RightEncoder_Count = 0;
        if (currentBox == 2 && nextBox == 3)
        {
          stage = 1;        
        }

        else if (LightReading == 0)
        {
          nextBox = 3;
          stage = 1;        
        }

        else if (LightReading == 1)
        {
          Serial.println(outgoing);
          stage = 0;
        }         
      }      

      break;  
    }

  case 21: //wait until the arm is done delivering
    {
      servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
      servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);
      OutgoingLight();
      CharliePlexM::ul_LeftEncoder_Count = 0;
      CharliePlexM::ul_RightEncoder_Count = 0;   

      incoming = Serial.parseInt();        
      if (incoming == 69 || outgoing == 1)
      {

        if (currentBox == 1 && nextBox == 2)
        {
          stage = 16;
          currentBox = 2;
          incoming = 1;
          outgoing = 1;
        }

        if (currentBox == 1 && nextBox == 3)
        {
          stage = 6;
          currentBox = 3;
          incoming = 1;
          outgoing = 1;
        }

        if (currentBox == 2 && nextBox == 1)
        {
          stage = 20;
          currentBox = 1;
          incoming = 1;
          outgoing = 1;
        }

        if (currentBox == 3 && nextBox == 1)
        {
          stage = 12;
          currentBox = 1;
          incoming = 1;
          outgoing = 1;
        }

        if (currentBox == 2 && nextBox == 3)
        {
          stage = 6;
          currentBox = 3;
          incoming = 1;
          outgoing = 1;
        }

        if (currentBox == 3 && nextBox == 2)
        {
          stage = 16;
          currentBox = 2;
          incoming = 1;
          outgoing = 1; 
        }

      }

      break;
    }

  }//end of switch statement
}


// measure distance to target using ultrasonic sensors 
int Ping_LeftOne()
{
  while (millis() - u1_PingTime_LeftOne < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_LeftOne, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_LeftOne, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_LeftOne = pulseIn(ci_Ultrasonic_LeftOne_Data, HIGH, 10000);
  distance_LeftOne = ul_Echo_Time_LeftOne/58;
  u1_PingTime_LeftOne = millis();

  // Print Sensor Readings
#ifdef DEBUG_ULTRASONIC
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_LeftOne/58); //divide time by 58 to get distance in cm 
#endif

  return distance_LeftOne; // return the distance in centimeters
}

int Ping_LeftTwo()
{
  while (millis() - u1_PingTime_LeftTwo < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_LeftTwo, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_LeftTwo, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_LeftTwo = pulseIn(ci_Ultrasonic_LeftTwo_Data, HIGH, 10000);
  distance_LeftTwo = ul_Echo_Time_LeftTwo/58;
  u1_PingTime_LeftTwo = millis();

  // Print Sensor Readings
#ifdef DEBUG_ULTRASONIC
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_LeftTwo/58); //divide time by 58 to get distance in cm 
#endif

  return distance_LeftTwo; // return the distance in centimeters
}

void Ping_Left()
{
  Ping_LeftOne();
  delayMicroseconds(10);
  Ping_LeftTwo();
  return;  
}


int Ping_Front()
{
  while (millis() - u1_PingTime_Front < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_Front, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_Front, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_Front = pulseIn(ci_Ultrasonic_Front_Data, HIGH, 10000);
  distance_Front = ul_Echo_Time_Front/58;
  u1_PingTime_Front = millis();

  // Print Sensor Readings
#ifdef DEBUG_FRONT_US
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_Front/58); //divide time by 58 to get distance in cm 
#endif

  return distance_Front; // return the distance in centimeters
}

int Ping_Back()
{
  while (millis() - u1_PingTime_Back < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_Back, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_Back, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_Back = pulseIn(ci_Ultrasonic_Back_Data, HIGH, 10000);
  distance_Back = ul_Echo_Time_Back/58;
  u1_PingTime_Back = millis();

  // Print Sensor Readings
#ifdef DEBUG_ULTRASONIC
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_Back/58); //divide time by 58 to get distance in cm 
#endif

  return distance_Back; // return the distance in centimeters
}


void DifferenceLeftOne(/*boolean goingForwards*/)
{
  distance_LeftOneDiff = distance_LeftOne - distance_LeftOneOld;

  /* if (distance_LeftDiff < 0) distance_LeftOneOld = distance_LeftOneOld*(-1);
   
   if (distance_LeftDiff > 1)
   {
   gapCount++;
   
   addtoDoor = false;
   
   if (gapCount%2 == 0){
   addtoDoor = true;
   }
   
   if (addtoDoor) doorCount++;
   
   }*/

  distance_LeftOneOld = distance_LeftOne;

#ifdef DEBUG_DIFF
  Serial.println("Difference: ");
  Serial.println(distance_Diff);
#endif

    return;
}

boolean Debounce_LeftOne()
{
  for (int i = 1; i < 3; i++){  
    Ping_LeftOne();

    distance_LeftOneDebounce = distance_LeftOne - distance_LeftOneOld;

    if (distance_LeftOneDebounce < 0) distance_LeftOneDebounce = distance_LeftOneDebounce*(-1);

    distance_LeftOneOld = distance_LeftOne;

    if (distance_LeftOneDebounce > 5) return false;
  }

  return true;
}

boolean Debounce_LeftTwo()
{
  for (int i = 1; i < 3; i++){  
    Ping_LeftTwo();

    distance_LeftTwoDebounce = distance_LeftTwo - distance_LeftTwoOld;

    if (distance_LeftTwoDebounce < 0) distance_LeftTwoDebounce = distance_LeftTwoDebounce*(-1);

    distance_LeftTwoOld = distance_LeftTwo;

    if (distance_LeftTwoDebounce > 5) return false;
  }

  return true;
}

boolean Debounce_Left()
{
  for (int i = 1; i < 3; i++){  
    Ping_Left();

    distance_LeftOneDebounce = distance_LeftOne - distance_LeftOneOld;

    if (distance_LeftOneDebounce < 0) distance_LeftOneDebounce = distance_LeftOneDebounce*(-1);

    distance_LeftOneOld = distance_LeftOne;

    distance_LeftTwoDebounce = distance_LeftTwo - distance_LeftTwoOld;

    if (distance_LeftTwoDebounce < 0) distance_LeftTwoDebounce = distance_LeftTwoDebounce*(-1);

    distance_LeftTwoOld = distance_LeftTwo;

    if (distance_LeftOneDebounce > 5 && distance_LeftTwoDebounce > 5) return false;
  }

  return true;  
}

boolean Debounce_Front()
{
  for (int i = 1; i < 3; i++){  
    Ping_Front();

    distance_FrontDiff = distance_Front - distance_FrontOld;

    if (distance_FrontDiff < 0) distance_FrontOld = distance_FrontOld*(-1);

    distance_FrontOld = distance_Front;

    if (distance_FrontDiff > 5) return false;

  }

  return true;
}

boolean Debounce_Back()
{
  for (int i = 1; i < 3; i++){
    Ping_Back();

    distance_BackDiff = distance_Back - distance_BackOld;

    if (distance_BackDiff < 0) distance_BackOld = distance_BackOld*(-1);

    distance_BackOld = distance_Back;

    if (distance_BackDiff > 5) return false;

  }
  return true;
}


void OutgoingLight()
{
  servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
  servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop);  
  LightReading = 0;
  for (int i = 1; i < 1000; i++){
    LightSensorFront = analogRead(ci_Light_Sensor_Front);

    if (LightSensorFront < 30) LightReading = 1;
  }
}

void whiskerCounter1()
{
  whisker1 = digitalRead (whiskerPort1);
  if (whiskerOld1 == HIGH && whisker1 == LOW)
  {
    whiskerCount1++;
  }
  whiskerOld1 = whisker1;
  return;
}

void whiskerCounter2()
{
  whisker2 = digitalRead (whiskerPort2);
  if (whiskerOld2 == HIGH && whisker2 == LOW)
  {
    whiskerCount2++;
  }
  whiskerOld2 = whisker2;
  return;
}






