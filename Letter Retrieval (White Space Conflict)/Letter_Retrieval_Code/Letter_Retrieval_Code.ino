/*

 Editted by Justin
 1) added communication function
 2) commented out all other Serial communication; can't serial print if we're using TX+RX
 3) assuming that the top will be the board that's starting the process, due to calibration needs
 
 */

#include <Servo.h>
#include <EEPROM.h>
#include <uSTimer2.h>
#include <CharliePlexM.h>

//Declared Variables

int start = 0;

int left = 0;    //Line Tracker Readings - 0 is light, 1 is dark
int center = 0;
int right = 0;

int ArmEncoderRevolution = 1450; //1415 was too much on full battery. Note that battery seems to have some effect on the encoder values.
//originally = 1390
int forward = 1700;
int stopped = 1500; //Speeds for motor
int reverse = 1300;

int mailbox = 0;    //Address of 1, 2, 3, or 4 of read letter's destination
//a value of 69 is default, used to tell the base that there's no mail
//a value of 70 is sent if at the first mailbox it doesn't retrieve any mail

int LetterStage = 69;  //Action stage where 1 is retrieval and 2 is reading
//a value of 69 is the default value, used only during setup


//=====================

//Port Pin Constants

const int ci_encoder_Pin_A[2] = {
  5,6};

//Charlieplexed LEDs 

const int ci_Charlieplex_LED1 = 4;
const int ci_Charlieplex_LED2 = 7;
const int ci_Charlieplex_LED3 = 12;
const int ci_Charlieplex_LED4 = 13;

const int ci_Mode_Button = 7;

//Line Tracker Pin Assignments
const int ci_Left_Line_Tracker = A0;
const int ci_Middle_Line_Tracker = A1;
const int ci_Right_Line_Tracker = A2;

// Charlieplexing LED assignments
const int ci_Left_Line_Tracker_LED = 1;
const int ci_Middle_Line_Tracker_LED = 4;
const int ci_Right_Line_Tracker_LED = 7;
const int ci_Indicator_LED = 3;
const int ci_Heartbeat_LED = 12;

// EEPROM addresses
const int ci_Left_Line_Tracker_Dark_Address_L = 4;
const int ci_Left_Line_Tracker_Dark_Address_H = 5;
const int ci_Left_Line_Tracker_Light_Address_L = 6;
const int ci_Left_Line_Tracker_Light_Address_H = 7;
const int ci_Middle_Line_Tracker_Dark_Address_L = 8;
const int ci_Middle_Line_Tracker_Dark_Address_H = 9;
const int ci_Middle_Line_Tracker_Light_Address_L = 10;
const int ci_Middle_Line_Tracker_Light_Address_H = 11;
const int ci_Right_Line_Tracker_Dark_Address_L = 12;
const int ci_Right_Line_Tracker_Dark_Address_H = 13;
const int ci_Right_Line_Tracker_Light_Address_L = 14;
const int ci_Right_Line_Tracker_Light_Address_H = 15;

const int ci_Line_Tracker_Calibration_Interval = 100;
const int ci_Line_Tracker_Cal_Measures = 20;


//variables

//Line Tracker Stuff
byte b_LowByte;
byte b_HighByte;
unsigned int ui_Left_Line_Tracker_Data;
unsigned int ui_Middle_Line_Tracker_Data;
unsigned int ui_Right_Line_Tracker_Data;


unsigned int ui_Cal_Count;
unsigned int ui_Left_Line_Tracker_Dark;
unsigned int ui_Left_Line_Tracker_Light;
unsigned int ui_Middle_Line_Tracker_Dark;
unsigned int ui_Middle_Line_Tracker_Light;
unsigned int ui_Right_Line_Tracker_Dark;
unsigned int ui_Right_Line_Tracker_Light;
unsigned int ui_Line_Tracker_Tolerance = 10;

//Encoder Stuff
const int ci_Num_Encoders = 2;
const int ci_Encoder_Steps_Per_Revolution = 90;
unsigned long ul_encoder_Pos[ci_Num_Encoders] = {
  0,0};
unsigned long ul_old_Encoder_Pos[ci_Num_Encoders] = {
  0,0};



//Other Stuff
unsigned long ul_3_Second_timer = 0;
unsigned long ul_Display_Time;
unsigned long ul_Calibration_Time;

unsigned int  ui_Robot_State_Index = 0;

//Arm Motor
const int ci_Arm_Motor = 8;
Servo servo_ArmMotor;



//0123456789ABCDEF
unsigned int  ui_Mode_Indicator[6] = {
  0x00,    //B0000000000000000,  //stop
  0x00FF,  //B0000000011111111,  //Run
  0x0F0F,  //B0000111100001111,  //Calibrate line tracker light level
  0x3333,  //B0011001100110011,  //Calibrate line tracker dark level
  0xAAAA,  //B1010101010101010,  //Calibrate motors
  0xFFFF}; //B1111111111111111};
unsigned int  ui_Mode_Indicator_Index = 0;

//display Bits 0,1,2,3, 4, 5, 6,  7,  8,  9,  10,  11,  12,  13,   14,   15
int  iArray[16] = {
  1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,65536};
int  iArrayIndex = 0;

boolean bt_Heartbeat = true;
boolean bt_3_S_Time_Up = false;
boolean bt_Do_Once = false;
boolean bt_Cal_Initialized = false;


void setup() {

  Serial.begin(9600);

  CharliePlexM::setBtn(ci_Charlieplex_LED1,ci_Charlieplex_LED2,ci_Charlieplex_LED3,ci_Charlieplex_LED4,ci_Mode_Button);
  CharliePlexM::setEncoders(ci_encoder_Pin_A[0],ci_encoder_Pin_A[1]);


  pinMode(ci_Middle_Line_Tracker, INPUT);
  pinMode(ci_Left_Line_Tracker, INPUT);
  pinMode(ci_Right_Line_Tracker, INPUT);


  // set up arm motors
  pinMode(ci_Arm_Motor, OUTPUT);
  servo_ArmMotor.attach(ci_Arm_Motor);


  // read saved values from EEPROM
  b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Left_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Left_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Middle_Line_Tracker_Dark = word(b_HighByte, b_LowByte); 
  b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Middle_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Right_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Right_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  ui_Line_Tracker_Tolerance = 75;
}


void loop()
{

  if((millis() - ul_3_Second_timer) > 3000)
  {
    bt_3_S_Time_Up = true;
  }

  // button-based mode selection
  if(CharliePlexM::ui_Btn)
  {
    if(bt_Do_Once == false)
    {
      bt_Do_Once = true;
      ui_Robot_State_Index++;
      ui_Robot_State_Index = ui_Robot_State_Index & 7;
      ul_3_Second_timer = millis();
      bt_3_S_Time_Up = false;
      bt_Cal_Initialized = false;
    }
  }
  else
  {
    bt_Do_Once = LOW;
  }

  //modes
  switch(ui_Robot_State_Index)
  {//========================================================================================================
  case 0:    //Line Tracker Debug
    {
      //BeltTesting();
      ui_Mode_Indicator_Index = 0;
      readLineTrackers();


#ifdef DEBUG_LINE_TRACKERS
      Serial.print("Trackers: Left = ");
      Serial.print(ui_Left_Line_Tracker_Data,DEC);
      Serial.print(" and ");
      Serial.print(left, DEC);
      Serial.print(", Middle = ");
      Serial.print(ui_Middle_Line_Tracker_Data,DEC);
      Serial.print(" and ");
      Serial.print(center, DEC);
      Serial.print(", Right = ");
      Serial.print(ui_Right_Line_Tracker_Data,DEC);
      Serial.print(" and ");
      Serial.println(right, DEC);
#endif

      break;
    }//========================================================================================================

  case 1: //Letter Retreival
    {
      switch(LetterStage){

      case 0: //waiting for the base to inform it what to do
        {
          LetterStage = Serial.parseInt();
          break;  
        }

      case 1: //Pick up mail
        {
          readLineTrackers();
          CharliePlexM::ul_LeftEncoder_Count=0;
          
          if (left==1 && center==1 && right==1 & start != 0) //Reading all black, rotating belt to retrieve
          {                                                  //the normal case
            servo_ArmMotor.writeMicroseconds(reverse);

            while (left==1 && center==1 && right==1)
            {
              ul_encoder_Pos[0] = CharliePlexM::ul_LeftEncoder_Count;

              if(ul_encoder_Pos[0] > ArmEncoderRevolution)
              {
                CharliePlexM::ul_LeftEncoder_Count = 0;
              }
              
              readLineTrackers();     
              
            }
          }
          
          
          if (left==1 && center==1 && right==1 & start == 0) //Reading all black, rotating belt to retrieve
          {                                                  //initial case, accounts for no mail in the first slot
            servo_ArmMotor.writeMicroseconds(reverse);

            while (ul_encoder_Pos[0] < (ArmEncoderRevolution))
            {
              ul_encoder_Pos[0] = CharliePlexM::ul_LeftEncoder_Count;

              readLineTrackers();      
            }

            if ((ul_encoder_Pos[0] >= (ArmEncoderRevolution)) && (left == 1 && center == 1 && right == 1))
            {
              servo_ArmMotor.writeMicroseconds(stopped);
              CharliePlexM::ul_LeftEncoder_Count = 0;
              ul_encoder_Pos[0] = 0;

              mailbox = 70;
              LetterStage = 0;
              Serial.println(mailbox);
            }
            start++;
          }


          if(!(left==1 && center==1 && right==1))
          {
            while(ul_encoder_Pos[0] < (ArmEncoderRevolution))
            {
              ul_encoder_Pos[0] = CharliePlexM::ul_LeftEncoder_Count;
            }

            servo_ArmMotor.writeMicroseconds(stopped);
            CharliePlexM::ul_LeftEncoder_Count = 0;
            ul_encoder_Pos[0] = 0;

            LetterStage++;
          }

          break;
        }

      case 2: //Read barcode
        {

          ScanCode();

          LetterStage = 0;
          Serial.println(mailbox);         

          break;
        }

      case 4: //Send out the Mail
        {

          CharliePlexM::ul_LeftEncoder_Count=0;
          ul_encoder_Pos[0] = CharliePlexM::ul_LeftEncoder_Count;
          delay(1000);

          servo_ArmMotor.writeMicroseconds(forward);

          while(ul_encoder_Pos[0]<1*ArmEncoderRevolution)
          {
            ul_encoder_Pos[0] = CharliePlexM::ul_LeftEncoder_Count;
          } 

          servo_ArmMotor.writeMicroseconds(stopped);
          CharliePlexM::ul_LeftEncoder_Count = 0;
          ul_encoder_Pos[0] = 0;

          LetterStage = 0;
          mailbox = 69;
          Serial.println(mailbox);

          break;
        }

      case 69: //the initial case, used to start off the communication between the boards
        {
          LetterStage = 0;
          mailbox = 69;
          Serial.println(mailbox);
          break;
        }
      }

      break;
    }

  case 2:    //Calibrate line tracker light levels after 3 seconds
    {
      if(bt_3_S_Time_Up)
      {
        if(!bt_Cal_Initialized)
        {
          bt_Cal_Initialized = true;
          ui_Left_Line_Tracker_Light = 0;
          ui_Middle_Line_Tracker_Light = 0;
          ui_Right_Line_Tracker_Light = 0;
          ul_Calibration_Time = millis();
          ui_Cal_Count = 0;
        }
        else if((millis() - ul_Calibration_Time) > ci_Line_Tracker_Calibration_Interval)
        {
          ul_Calibration_Time = millis();
          readLineTrackers();
          ui_Left_Line_Tracker_Light += ui_Left_Line_Tracker_Data;
          ui_Middle_Line_Tracker_Light += ui_Middle_Line_Tracker_Data;
          ui_Right_Line_Tracker_Light += ui_Right_Line_Tracker_Data;
          ui_Cal_Count++;
        }
        if(ui_Cal_Count == ci_Line_Tracker_Cal_Measures)
        {
          ui_Left_Line_Tracker_Light /= ci_Line_Tracker_Cal_Measures;
          ui_Middle_Line_Tracker_Light /= ci_Line_Tracker_Cal_Measures;
          ui_Right_Line_Tracker_Light /= ci_Line_Tracker_Cal_Measures;
#ifdef DEBUG_LINE_TRACKER_CALIBRATION
          Serial.print("Light Levels: Left = ");
          Serial.print(ui_Left_Line_Tracker_Light,DEC);
          Serial.print(", Middle = ");
          Serial.print(ui_Middle_Line_Tracker_Light,DEC);
          Serial.print(", Right = ");
          Serial.println(ui_Right_Line_Tracker_Light,DEC);
#endif           
          EEPROM.write(ci_Left_Line_Tracker_Light_Address_L, lowByte(ui_Left_Line_Tracker_Light));
          EEPROM.write(ci_Left_Line_Tracker_Light_Address_H, highByte(ui_Left_Line_Tracker_Light));
          EEPROM.write(ci_Middle_Line_Tracker_Light_Address_L, lowByte(ui_Middle_Line_Tracker_Light));
          EEPROM.write(ci_Middle_Line_Tracker_Light_Address_H, highByte(ui_Middle_Line_Tracker_Light));
          EEPROM.write(ci_Right_Line_Tracker_Light_Address_L, lowByte(ui_Right_Line_Tracker_Light));
          EEPROM.write(ci_Right_Line_Tracker_Light_Address_H, highByte(ui_Right_Line_Tracker_Light));
          ui_Robot_State_Index = 0;    // go back to Mode 0
        }
        ui_Mode_Indicator_Index = 2; 
      }
      break;
    }

  case 3:    // Calibrate line tracker dark levels after 3 seconds
    {
      if(bt_3_S_Time_Up)
      {
        if(!bt_Cal_Initialized)
        {
          bt_Cal_Initialized = true;
          ui_Left_Line_Tracker_Dark = 0;
          ui_Middle_Line_Tracker_Dark = 0;
          ui_Right_Line_Tracker_Dark = 0;
          ul_Calibration_Time = millis();
          ui_Cal_Count = 0;
        }
        else if((millis() - ul_Calibration_Time) > ci_Line_Tracker_Calibration_Interval)
        {
          ul_Calibration_Time = millis();
          readLineTrackers();
          ui_Left_Line_Tracker_Dark += ui_Left_Line_Tracker_Data;
          ui_Middle_Line_Tracker_Dark += ui_Middle_Line_Tracker_Data;
          ui_Right_Line_Tracker_Dark += ui_Right_Line_Tracker_Data;
          ui_Cal_Count++;
        }
        if(ui_Cal_Count == ci_Line_Tracker_Cal_Measures)
        {
          ui_Left_Line_Tracker_Dark /= ci_Line_Tracker_Cal_Measures;
          ui_Middle_Line_Tracker_Dark /= ci_Line_Tracker_Cal_Measures;
          ui_Right_Line_Tracker_Dark /= ci_Line_Tracker_Cal_Measures;
#ifdef DEBUG_LINE_TRACKER_CALIBRATION
          Serial.print("Dark Levels: Left = ");
          Serial.print(ui_Left_Line_Tracker_Dark,DEC);
          Serial.print(", Middle = ");
          Serial.print(ui_Middle_Line_Tracker_Dark,DEC);
          Serial.print(", Right = ");
          Serial.println(ui_Right_Line_Tracker_Dark,DEC);
#endif           
          EEPROM.write(ci_Left_Line_Tracker_Dark_Address_L, lowByte(ui_Left_Line_Tracker_Dark));
          EEPROM.write(ci_Left_Line_Tracker_Dark_Address_H, highByte(ui_Left_Line_Tracker_Dark));
          EEPROM.write(ci_Middle_Line_Tracker_Dark_Address_L, lowByte(ui_Middle_Line_Tracker_Dark));
          EEPROM.write(ci_Middle_Line_Tracker_Dark_Address_H, highByte(ui_Middle_Line_Tracker_Dark));
          EEPROM.write(ci_Right_Line_Tracker_Dark_Address_L, lowByte(ui_Right_Line_Tracker_Dark));
          EEPROM.write(ci_Right_Line_Tracker_Dark_Address_H, highByte(ui_Right_Line_Tracker_Dark));
          ui_Robot_State_Index = 0;    // go back to Mode 0
        }
        ui_Mode_Indicator_Index = 3;
      }
      break;
    }

  }
}

void readLineTrackers()
{
  ui_Left_Line_Tracker_Data = analogRead(ci_Left_Line_Tracker);
  ui_Middle_Line_Tracker_Data = analogRead(ci_Middle_Line_Tracker);
  ui_Right_Line_Tracker_Data = analogRead(ci_Right_Line_Tracker);

  if(ui_Left_Line_Tracker_Data > 600 /*(ui_Left_Line_Tracker_Dark - ui_Line_Tracker_Tolerance)*/)
  {
    CharliePlexM::Write(ci_Left_Line_Tracker_LED, HIGH);
    left = 1;
  }
  else
  { 
    CharliePlexM::Write(ci_Left_Line_Tracker_LED, LOW);
    left = 0;
  }
  if(ui_Middle_Line_Tracker_Data > 600/*(ui_Middle_Line_Tracker_Dark - ui_Line_Tracker_Tolerance)*/)
  {
    CharliePlexM::Write(ci_Middle_Line_Tracker_LED, HIGH);
    center = 1;
  }
  else
  { 
    CharliePlexM::Write(ci_Middle_Line_Tracker_LED, LOW);
    center = 0;
  }
  if(ui_Right_Line_Tracker_Data > 600 /*(ui_Right_Line_Tracker_Dark - ui_Line_Tracker_Tolerance)*/)
  {
    CharliePlexM::Write(ci_Right_Line_Tracker_LED, HIGH);
    right = 1;
  }
  else
  { 
    CharliePlexM::Write(ci_Right_Line_Tracker_LED, LOW);
    right = 0;
  }

#ifdef DEBUG_LINE_TRACKERS
  Serial.print("Trackers: Left = ");
  Serial.print(ui_Left_Line_Tracker_Data,DEC);
  Serial.print(", Middle = ");
  Serial.print(ui_Middle_Line_Tracker_Data,DEC);
  Serial.print(", Right = ");
  Serial.println(ui_Right_Line_Tracker_Data,DEC);
#endif

}

void ScanCode()
{
  readLineTrackers();

  if (left==0 && center==0 && right==0){
    mailbox = 1;
  }

  else if (left==0 && center==0 && right==1){
    mailbox = 2;
  }

  else if (left==0 && center==1 && right==0){
    mailbox = 3;
  }

  else if (left==0 && center==1 && right==1){
    mailbox = 4;
  }
}
