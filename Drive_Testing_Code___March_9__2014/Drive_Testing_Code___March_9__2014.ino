#include <Servo.h>
#include <EEPROM.h>

Servo servo_LeftMotor;
Servo servo_RightMotor;

const int ci_Left_Motor = 8;
const int ci_Right_Motor = 9;

int forward = 1700;
int stopped = 1500;
int reverse = 1300;

void setup()

{
   pinMode(ci_Left_Motor, OUTPUT);
  servo_LeftMotor.attach(ci_Left_Motor);
  pinMode(ci_Right_Motor, OUTPUT);
  servo_RightMotor.attach(ci_Right_Motor);
}

void loop()

{
  while (true)
  {
    
  servo_LeftMotor.writeMicroseconds(stopped); 
  servo_RightMotor.writeMicroseconds(stopped); 
  
  delay(1000);
  
  servo_LeftMotor.writeMicroseconds(forward); 
  servo_RightMotor.writeMicroseconds(forward); 
  
  delay(5000);
  
  servo_LeftMotor.writeMicroseconds(stopped); 
  servo_RightMotor.writeMicroseconds(stopped); 
  
  delay(1000);
  
  servo_LeftMotor.writeMicroseconds(reverse); 
  servo_RightMotor.writeMicroseconds(reverse); 
  
  delay(5000);
       
  }   
}
