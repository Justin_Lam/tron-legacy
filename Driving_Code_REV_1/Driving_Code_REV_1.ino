/*

 MSE 2202b Final Project
 Language: Arduino
 Authors: Tron: Legacy
 Date: 14/03/11
 
 REV 1 by Justin - 
 //Wall tracking/following code
 //some extra code for later that is commented out
 //only trying to drive foward and backwards using the wall
 
 */

#include <Servo.h>
#include <EEPROM.h>
#include <uSTimer2.h>
#include <CharliePlexM.h>

//declaration of motors and servos
Servo servo_LeftMotor;
Servo servo_RightMotor;
//Servo servo_BeltMotor;
//Servo servo_Bridge;

//only for quick testing
unsigned long clock = 0;
unsigned long now = 0;

//declaration of values for motors and servos
int forward = 1850; //max 2000
int stopped = 1500;
int reverse = 1150; //min 1000
//int bridgeAngle = 0; //where the integer is the angle of the servo

//declarations for ultrasonic sensors distances
unsigned long u1_PingTime_LeftOne = 0;
unsigned long distance_LeftOne = 10;

unsigned long u1_PingTime_LeftTwo = 0;
unsigned long distance_LeftTwo = 10;

// Uncomment keywords to enable debugging output

//#define DEBUG_MODE_DISPLAY
//#define DEBUG_MOTORS
//#define DEBUG_LINE_TRACKERS
//#define DEBUG_ENCODERS
//#define DEBUG_ULTRASONIC
//#define DEBUG_LINE_TRACKER_CALIBRATION
//#define DEBUG_MOTOR_CALIBRATION
//#define DEBUG_ARM

boolean bt_Motors_Enabled = true;

//port pin constants
//this is the default setup from Lab 04 for the most part
const int ci_encoder_Pin_A[2] = {
  2,3};
const int ci_Ultrasonic_LeftOne = 5; //input plug
const int ci_Ultrasonic_LeftOne_Data = 6; //output plug
const int ci_Mode_Button = 7; //we should move this to board two
const int ci_Left_Motor = 8;
const int ci_Right_Motor = 9;
const int ci_Ultrasonic_LeftTwo = 10; //input plug //these two replaced the arm and grip
const int ci_Ultrasonic_LeftTwo_Data = 11; //output plug
const int ci_Charlieplex_LED1 = 4;
const int ci_Charlieplex_LED2 = 7;
const int ci_Charlieplex_LED3 = 12;
const int ci_Charlieplex_LED4 = 13;
const int ci_Left_Line_Tracker = A0;
const int ci_Middle_Line_Tracker = A1;
const int ci_Right_Line_Tracker = A2;
//const int ci_Motor_Speed_Pot = A3;
//const int ci_Light_Sensor = A4;
//const int ci_Arm_Length_Pot = A5;

// Charlieplexing LED assignments
const int ci_Left_Line_Tracker_LED = 1;
const int ci_Middle_Line_Tracker_LED = 4;
const int ci_Right_Line_Tracker_LED = 7;
const int ci_Indicator_LED = 3;
const int ci_Heartbeat_LED = 12;

//constants

// EEPROM addresses
const int ci_Right_Motor_Offset_Address_L = 0;
const int ci_Right_Motor_Offset_Address_H = 1;
const int ci_Left_Motor_Offset_Address_L = 2;
const int ci_Left_Motor_Offset_Address_H = 3;
const int ci_Left_Line_Tracker_Dark_Address_L = 4;
const int ci_Left_Line_Tracker_Dark_Address_H = 5;
const int ci_Left_Line_Tracker_Light_Address_L = 6;
const int ci_Left_Line_Tracker_Light_Address_H = 7;
const int ci_Middle_Line_Tracker_Dark_Address_L = 8;
const int ci_Middle_Line_Tracker_Dark_Address_H = 9;
const int ci_Middle_Line_Tracker_Light_Address_L = 10;
const int ci_Middle_Line_Tracker_Light_Address_H = 11;
const int ci_Right_Line_Tracker_Dark_Address_L = 12;
const int ci_Right_Line_Tracker_Dark_Address_H = 13;
const int ci_Right_Line_Tracker_Light_Address_L = 14;
const int ci_Right_Line_Tracker_Light_Address_H = 15;

const int ci_Left_Motor_Stop = 1500; // 200 for brake mode; 1500 for stop
const int ci_Right_Motor_Stop = 1500;
const int ci_Grip_Motor_Open = 176; // Experiment to determine appropriate value
const int ci_Grip_Motor_Zero = 90; // "
const int ci_Grip_Motor_Closed = 140; // "
const int ci_Arm_Pot_Retracted = 1020; // "
const int ci_Arm_Pot_Extended = 400; // "
const int ci_Arm_Pot_Tolerance = 60; // "
const int ci_Display_Time = 500;
const int ci_Num_Encoders = 2;
const int ci_Encoder_Steps_Per_Revolution = 90;
const int ci_Line_Tracker_Calibration_Interval = 100;
const int ci_Line_Tracker_Cal_Measures = 20;
const int ci_Motor_Calibration_Time = 5000;

//variables
byte b_LowByte;
byte b_HighByte;
unsigned long ul_Echo_Time_LeftOne;
unsigned long ul_Echo_Time_LeftTwo;
unsigned int ui_Left_Line_Tracker_Data;
unsigned int ui_Middle_Line_Tracker_Data;
unsigned int ui_Right_Line_Tracker_Data;
unsigned int ui_Motors_Speed;
unsigned int ui_Left_Motor_Speed;
unsigned int ui_Right_Motor_Speed;
unsigned int ui_Arm_Length_Data;

volatile unsigned long ul_encoder_Count[ci_Num_Encoders] = {
  0,0};
unsigned long ul_encoder_Pos[ci_Num_Encoders] = {
  0,0};
unsigned long ul_old_Encoder_Pos[ci_Num_Encoders] = {
  0,0};

unsigned long ul_3_Second_timer = 0;
unsigned long ul_Display_Time;
unsigned long ul_Calibration_Time;

unsigned int ui_Right_Motor_Offset = 0;
unsigned int ui_Left_Motor_Offset = 0;

unsigned int ui_Cal_Count;
unsigned int ui_Left_Line_Tracker_Dark;
unsigned int ui_Left_Line_Tracker_Light;
unsigned int ui_Middle_Line_Tracker_Dark;
unsigned int ui_Middle_Line_Tracker_Light;
unsigned int ui_Right_Line_Tracker_Dark;
unsigned int ui_Right_Line_Tracker_Light;
unsigned int ui_Line_Tracker_Tolerance = 10;

unsigned int ui_Robot_State_Index = 0;
//0123456789ABCDEF
unsigned int ui_Mode_Indicator[6] = {
  0x00, //B0000000000000000, //stop
  0x00FF, //B0000000011111111, //Run
  0x0F0F, //B0000111100001111, //Calibrate line tracker light level
  0x3333, //B0011001100110011, //Calibrate line tracker dark level
  0xAAAA, //B1010101010101010, //Calibrate motors
  0xFFFF}; //B1111111111111111};
unsigned int ui_Mode_Indicator_Index = 0;

//display Bits 0,1,2,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
int iArray[16] = {
  1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,65536};
int iArrayIndex = 0;


int i_Left_Motor_Calibration = 0;
int i_Right_Motor_Calibration = 0;

boolean bt_Heartbeat = true;
boolean bt_3_S_Time_Up = false;
boolean bt_Do_Once = false;
boolean bt_Cal_Initialized = false;

void setup(){
  Serial.begin(9600);

  CharliePlexM::setBtn(ci_Charlieplex_LED1,ci_Charlieplex_LED2,ci_Charlieplex_LED3,ci_Charlieplex_LED4,ci_Mode_Button);

  // set up ultrasonic Left #1
  pinMode(ci_Ultrasonic_LeftOne, OUTPUT);
  pinMode(ci_Ultrasonic_LeftOne_Data, INPUT);

  // set up ultrasonic Left #2
  pinMode(ci_Ultrasonic_LeftTwo, OUTPUT);
  pinMode(ci_Ultrasonic_LeftTwo_Data, INPUT);

  // set up drive motors
  pinMode(ci_Left_Motor, OUTPUT);
  servo_LeftMotor.attach(ci_Left_Motor);
  pinMode(ci_Right_Motor, OUTPUT);
  servo_RightMotor.attach(ci_Right_Motor);

  /*

   // set up arm motors
   pinMode(ci_Arm_Motor, OUTPUT);
   servo_ArmMotor.attach(ci_Arm_Motor);
   pinMode(ci_Grip_Motor, OUTPUT);
   servo_GripMotor.attach(ci_Grip_Motor);
   servo_GripMotor.write(ci_Grip_Motor_Zero);
   
   */

  pinMode(ci_Middle_Line_Tracker, INPUT);
  pinMode(ci_Left_Line_Tracker, INPUT);
  pinMode(ci_Right_Line_Tracker, INPUT);

  //pinMode(ci_Motor_Speed_Pot, INPUT);
  //pinMode(ci_Arm_Length_Pot, INPUT);

  // read saved values from EEPROM
  b_LowByte = EEPROM.read(ci_Right_Motor_Offset_Address_L);
  b_HighByte = EEPROM.read(ci_Right_Motor_Offset_Address_H);
  ui_Right_Motor_Offset = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Left_Motor_Offset_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Motor_Offset_Address_H);
  ui_Left_Motor_Offset = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Left_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Left_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Left_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Middle_Line_Tracker_Dark = word(b_HighByte, b_LowByte); 
  b_LowByte = EEPROM.read(ci_Middle_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Middle_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Dark_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Right_Line_Tracker_Dark = word(b_HighByte, b_LowByte);
  b_LowByte = EEPROM.read(ci_Right_Line_Tracker_Light_Address_L);
  b_HighByte = EEPROM.read(ci_Left_Line_Tracker_Dark_Address_H);
  ui_Right_Line_Tracker_Light = word(b_HighByte, b_LowByte);
  ui_Line_Tracker_Tolerance = 75;
}

void loop(){
  //clock = millis();
  Ping_LeftOne();
  Ping_LeftTwo();
  if (millis() > 5000){
    servo_LeftMotor.writeMicroseconds(ci_Left_Motor_Stop); 
    servo_RightMotor.writeMicroseconds(ci_Right_Motor_Stop); 
  }

  //if LeftOne is too far and LeftTwo is too close, turn left
  if (distance_LeftOne > 12 && distance_LeftTwo < 8){ 
    servo_LeftMotor.writeMicroseconds(reverse); 
    servo_RightMotor.writeMicroseconds(forward);
  }
  //if LeftOne is too close and LeftTwo is too far, turn right
  else if (distance_LeftTwo > 12 && distance_LeftOne < 8){ 
    servo_LeftMotor.writeMicroseconds(forward); 
    servo_RightMotor.writeMicroseconds(reverse);
  }
  //otherwise, drive forward
  else{
    servo_LeftMotor.writeMicroseconds(forward); 
    servo_RightMotor.writeMicroseconds(forward);
  }

}

// measure distance to target using ultrasonic sensors 
int Ping_LeftOne()
{
  while (millis() - u1_PingTime_LeftOne < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_LeftOne, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_LeftOne, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_LeftOne = pulseIn(ci_Ultrasonic_LeftOne_Data, HIGH, 10000);
  distance_LeftOne = ul_Echo_Time_LeftOne/58;
  u1_PingTime_LeftOne = millis();

  // Print Sensor Readings
#ifdef DEBUG_ULTRASONIC
  Serial.print("Time (microseconds): ");
  Serial.print(ul_Echo_Time_LeftOne, DEC);
  Serial.print(", Inches: ");
  Serial.print(ul_Echo_Time_LeftOne/148); //divide time by 148 to get distance in inches
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_LeftOne/58); //divide time by 58 to get distance in cm 
#endif

  return distance_LeftOne; // return the distance in centimeters
}

int Ping_LeftTwo()
{
  while (millis() - u1_PingTime_LeftTwo < 16) delay(2);
  //Ping Ultrasonic
  //Send the Ultrasonic Range Finder a 10 microsecond pulse per tech spec
  digitalWrite(ci_Ultrasonic_LeftTwo, HIGH);

  delayMicroseconds(10); //The 10 microsecond pause where the pulse in "high"

  digitalWrite(ci_Ultrasonic_LeftTwo, LOW);

  //use command pulseIn to listen to Ultrasonic_Data pin to record the
  //time that it takes from when the Pin goes HIGH until it goes LOW 
  ul_Echo_Time_LeftTwo = pulseIn(ci_Ultrasonic_LeftTwo_Data, HIGH, 10000);
  distance_LeftTwo = ul_Echo_Time_LeftTwo/58;
  u1_PingTime_LeftTwo = millis();

  // Print Sensor Readings
#ifdef DEBUG_ULTRASONIC
  Serial.print("Time (microseconds): ");
  Serial.print(ul_Echo_Time_LeftTwo, DEC);
  Serial.print(", Inches: ");
  Serial.print(ul_Echo_Time_LeftTwo/148); //divide time by 148 to get distance in inches
  Serial.print(", cm: ");
  Serial.println(ul_Echo_Time_LeftTwo/58); //divide time by 58 to get distance in cm 
#endif

  return distance_LeftTwo; // return the distance in centimeters
}

